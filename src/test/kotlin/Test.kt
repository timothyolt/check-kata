@file:OptIn(ExperimentalStdlibApi::class)
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class TheTests {
    @Test
    fun oneDollar() {
        assertEquals("one dollar", Check(1.00).spellItOut())
    }

    @Test
    fun twoDollars() {
        assertEquals("two dollars", Check(2.00).spellItOut())
    }

    @Test
    fun zeroDollars() {
        assertEquals("zero dollars", Check(0.00).spellItOut())
    }

    @Test
    fun tenDollars() {
        assertEquals("ten dollars", Check(10.00).spellItOut())
    }

    @Test
    fun twentyOneDollars() {
        assertEquals("twenty-one dollars", Check(21.00).spellItOut())
    }

    @Test
    fun twentyNineDollars() {
        assertEquals("twenty-nine dollars", Check(29.00).spellItOut())
    }

    @Test
    fun oneHundredAndOneDollars() {
        assertEquals("one hundred one dollars", Check(101.00).spellItOut())
    }

    @Test
    fun oneHundredAndTwentyOneDollars() {
        assertEquals("one hundred twenty-one dollars", Check(121.00).spellItOut())
    }

    @Test
    fun oneHundredDollars() {
        assertEquals("one hundred dollars", Check(100.00).spellItOut())
    }

    @Test
    fun oneThousandOneHundredDollars() {
        assertEquals("one thousand one hundred dollars", Check(1100.00).spellItOut())
    }

    @Test
    fun oneThousandDollars() {
        assertEquals("one thousand dollars", Check(1000.00).spellItOut())
    }

    @Test
    fun oneThousandTwentyOneDollars() {
        assertEquals("one thousand twenty-one dollars", Check(1021.00).spellItOut())
    }

    @Test
    fun oneThousandOneDollars() {
        assertEquals("one thousand one dollars", Check(1001.00).spellItOut())
    }
}

class Check(val d: Double) {
    fun spellItOut() = whole() + " dollar" + plurality()

    private fun whole() = chunk(d.toInt())

    private fun chunk(i: Int): String = when (i) {
        0 -> "zero"
        1 -> "one"
        2 -> "two"
        9 -> "nine"
        10 -> "ten"
        in 21..29 -> "twenty-" + chunk(i - 20)
        in 100..999 -> powerOf10(i, 100, "hundred")
        in 1000..9999 -> powerOf10(i, 1000, "thousand")
        else -> TODO()
    }

    private fun powerOf10(i: Int, powerOf10: Int, spelling: String): String {
        val digit = i / powerOf10
        val remainder = i % powerOf10
        return chunk(digit) + " " + spelling + if (remainder > 0) " " + chunk(remainder) else ""
    }

    private fun plurality() = if (d in 1.0..<2.0) "" else "s"
}

/*

1.00 -> One dollar
2.00 -> Two dollars
3.00 -> Three dollars
10.00 -> Ten dollars
1.03 -> One dollar and 03/100
1.59 -> One dollar and 59/100
0.75 -> Zero dollars and 75/100
152.22 -> One hundred fifty-two dollars and 22/100

 */