fun main(args: Array<String>) {
    println("Hello World!")

    // Try adding program arguments via Run/Debug configuration.
    // Learn more about running applications: https://www.jetbrains.com/help/idea/running-applications.html.
    println("Program arguments: ${args.joinToString()}")
}

/*

Function that takes one parameter thats a double

1.00 -> One dollar
2.00 -> Two dollars
3.00 -> Three dollars
10.00 -> Ten dollars
1.03 -> One dollar and 03/100
1.59 -> One dollar and 59/100
0.75 -> Zero dollars and 75/100
152.22 -> One hundred fifty-two dollars and 22/100



Extra Credit: take two parameters (amount, currency which defaults to “USD”)  and depend on an ICurrencyRateService in the constructor.  Create your own mock CurrencyRateService that has a function that takes a currency and returns a rate to convert to that currency.

(1.00, USD)
	CurrencyRateService.getCurrentExchangeRate(“USD”) -> returns 1.00
	One dollar

(1.00, EUR)
	CurrencyRateService.getCurrentExchangeRate(“EUR”) -> returns 1.02
	One euro and 02/100

(2.50, EUR)
	CurrencyRateService.getCurrentExchangeRate(“EUR”) -> returns 1.02
	Two euros and 55/100

 */